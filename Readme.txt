Here is how this project built:

Go to the Building PDFium website:
https://code.google.com/p/pdfium/wiki/Build
And follow the instructions.

Here is what I did:
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=`pwd`/depot_tools:"$PATH"

mkdir pdfium
cd pdfium
gclient config --name . --unmanaged https://pdfium.googlesource.com/pdfium.git
gclient sync


The only difference is the following command:
build/gyp_pdfium -Dtarget_arch=ia32

Then open the pdfium folder, you can see the Xcode project file pdfium.xcodeproj, open it with Xcode. 

You need to change the project pdfium building setting: Architecture changes to 32-bit Intel(i386). Also change this setting in every project inside projects folder.

Then you click run button to generate all static libraries.
Long waiting is normal for building, 15 minutes probably, and 1G hard disk space is required.

After building, you should copy all pdfium/xcodebuild/Debug/*.a file to iSeePdfium project iSeePdfium/libpdfium/lib/
Also you need to copy the header files to iSeePdfium project:
pdfium/public/*		-> 	iSeePdfium/libpdfium/public/
pdfium/v8/include/* 	-> 	iSeePdfium/libpdfium/v8/include/

Then you should be able to compile iSeePdfium project and get pdfium.bundle.
