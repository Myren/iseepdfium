// Copyright (c) 2015 Pieter van Ginkel. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "libpdfium/public/fpdfview.h"
#include "libpdfium/public/fpdf_formfill.h"
#include "libpdfium/v8/include/v8.h"
#include "libpdfium/v8/include/libplatform/libplatform.h"
#include "libpdfium/public/fpdfview.h"
#include "libpdfium/public/fpdf_text.h"

extern "C"
{
    DLLEXPORT FPDF_DOCUMENT STDCALL FPDF_LoadMemDocument_Wraper(const void* data_buf, int size, FPDF_BYTESTRING password);
    DLLEXPORT void STDCALL FPDF_CloseDocument_Wraper(FPDF_DOCUMENT document);
    DLLEXPORT int STDCALL FPDF_GetPageCount_Wraper(FPDF_DOCUMENT document);
    DLLEXPORT unsigned long STDCALL FPDF_GetDocPermissions_Wraper(FPDF_DOCUMENT document);
    DLLEXPORT FPDF_FORMHANDLE STDCALL FPDFDOC_InitFormFillEnvironment_Wraper(FPDF_DOCUMENT document, FPDF_FORMFILLINFO* formInfo);
    DLLEXPORT void STDCALL FPDF_SetFormFieldHighlightColor_Wraper(FPDF_FORMHANDLE hHandle, int fieldType, unsigned long color);
    DLLEXPORT void STDCALL FPDF_SetFormFieldHighlightAlpha_Wraper(FPDF_FORMHANDLE hHandle, unsigned char alpha);
    DLLEXPORT void STDCALL FORM_DoDocumentJSAction_Wraper(FPDF_FORMHANDLE hHandle);
    DLLEXPORT void STDCALL FORM_DoDocumentOpenAction_Wraper(FPDF_FORMHANDLE hHandle);
    DLLEXPORT void STDCALL FPDFDOC_ExitFormFillEnvironment_Wraper(FPDF_FORMHANDLE hHandle);
    DLLEXPORT void STDCALL FORM_DoDocumentAAction_Wraper(FPDF_FORMHANDLE hHandle, int aaType);
    DLLEXPORT FPDF_PAGE STDCALL FPDF_LoadPage_Wraper(FPDF_DOCUMENT document, int page_index);
    DLLEXPORT FPDF_TEXTPAGE STDCALL FPDFText_LoadPage_Wraper(FPDF_PAGE page);
    DLLEXPORT void STDCALL FORM_OnAfterLoadPage_Wraper(FPDF_PAGE page, FPDF_FORMHANDLE hHandle);
    DLLEXPORT void STDCALL FORM_DoPageAAction_Wraper(FPDF_PAGE page, FPDF_FORMHANDLE hHandle, int aaType);
    DLLEXPORT double STDCALL FPDF_GetPageWidth_Wraper(FPDF_PAGE page);
    DLLEXPORT double STDCALL FPDF_GetPageHeight_Wraper(FPDF_PAGE page);
    DLLEXPORT void STDCALL FORM_OnBeforeClosePage_Wraper(FPDF_PAGE page, FPDF_FORMHANDLE hHandle);
    DLLEXPORT void STDCALL FPDFText_ClosePage_Wraper(FPDF_TEXTPAGE text_page);
    DLLEXPORT void STDCALL FPDF_ClosePage_Wraper(FPDF_PAGE page);
#ifdef _WIN32
    // Function: FPDF_RenderPage
    //          Render contents in a page to a device (screen, bitmap, or printer).
    //          This function is only supported on Windows system.
    // Parameters:
    //          dc          -   Handle to device context.
    //          page        -   Handle to the page. Returned by FPDF_LoadPage function.
    //          start_x     -   Left pixel position of the display area in the device coordinate.
    //          start_y     -   Top pixel position of the display area in the device coordinate.
    //          size_x      -   Horizontal size (in pixels) for displaying the page.
    //          size_y      -   Vertical size (in pixels) for displaying the page.
    //          rotate      -   Page orientation: 0 (normal), 1 (rotated 90 degrees clockwise),
    //                              2 (rotated 180 degrees), 3 (rotated 90 degrees counter-clockwise).
    //          flags       -   0 for normal display, or combination of flags defined above.
    // Return value:
    //          None.
    //
    DLLEXPORT void STDCALL FPDF_RenderPage_Wraper(HDC dc, FPDF_PAGE page, int start_x, int start_y, int size_x, int size_y, int rotate, int flags);
#endif
    DLLEXPORT void STDCALL FPDF_RenderPageBitmap_Wraper(FPDF_BITMAP bitmap, FPDF_PAGE page, int start_x, int start_y, int size_x, int size_y, int rotate, int flags);
    DLLEXPORT int STDCALL FPDF_GetPageSizeByIndex_Wraper(FPDF_DOCUMENT document, int page_index, double* width, double* height);
    DLLEXPORT FPDF_BITMAP STDCALL FPDFBitmap_CreateEx_Wraper(int width, int height, int format, void* first_scan, int stride);
    DLLEXPORT void STDCALL FPDFBitmap_FillRect_Wraper(FPDF_BITMAP bitmap, int left, int top, int width, int height, FPDF_DWORD color);
    DLLEXPORT void STDCALL FPDFBitmap_Destroy_Wraper(FPDF_BITMAP bitmap);
    DLLEXPORT FPDF_SCHHANDLE STDCALL FPDFText_FindStart_Wraper(FPDF_TEXTPAGE text_page, FPDF_WIDESTRING findwhat, unsigned long flags, int start_index);
    DLLEXPORT int STDCALL FPDFText_GetSchResultIndex_Wraper(FPDF_SCHHANDLE handle);
    DLLEXPORT int STDCALL FPDFText_GetSchCount_Wraper(FPDF_SCHHANDLE handle);
    DLLEXPORT int STDCALL FPDFText_GetText_Wraper(FPDF_TEXTPAGE text_page, int start_index, int count, unsigned short* result);
    DLLEXPORT void STDCALL FPDFText_GetCharBox_Wraper(FPDF_TEXTPAGE text_page, int index, double* left, double* right, double* bottom, double* top);
    DLLEXPORT FPDF_BOOL STDCALL FPDFText_FindNext_Wraper(FPDF_SCHHANDLE handle);
    DLLEXPORT void STDCALL FPDFText_FindClose_Wraper(FPDF_SCHHANDLE handle);
}

DLLEXPORT FPDF_DOCUMENT STDCALL FPDF_LoadMemDocument_Wraper(const void* data_buf,
                                                       int size, FPDF_BYTESTRING password)
{
    return FPDF_LoadMemDocument(data_buf, size, password);
}

DLLEXPORT void STDCALL FPDF_CloseDocument_Wraper(FPDF_DOCUMENT document)
{
    FPDF_CloseDocument(document);
}

DLLEXPORT int STDCALL FPDF_GetPageCount_Wraper(FPDF_DOCUMENT document)
{
    return FPDF_GetPageCount(document);
}

DLLEXPORT unsigned long STDCALL FPDF_GetDocPermissions_Wraper(FPDF_DOCUMENT document)
{
    return FPDF_GetDocPermissions(document);
}

DLLEXPORT FPDF_FORMHANDLE STDCALL FPDFDOC_InitFormFillEnvironment_Wraper(FPDF_DOCUMENT document, FPDF_FORMFILLINFO* formInfo)
{
    return FPDFDOC_InitFormFillEnvironment(document, formInfo);
}

DLLEXPORT void STDCALL FPDF_SetFormFieldHighlightColor_Wraper(FPDF_FORMHANDLE hHandle, int fieldType, unsigned long color)
{
    FPDF_SetFormFieldHighlightColor(hHandle, fieldType, color);
}

DLLEXPORT void STDCALL FPDF_SetFormFieldHighlightAlpha_Wraper(FPDF_FORMHANDLE hHandle, unsigned char alpha)
{
    FPDF_SetFormFieldHighlightAlpha(hHandle, alpha);
}

DLLEXPORT void STDCALL FORM_DoDocumentJSAction_Wraper(FPDF_FORMHANDLE hHandle)
{
    FORM_DoDocumentJSAction(hHandle);
}

DLLEXPORT void STDCALL FORM_DoDocumentOpenAction_Wraper(FPDF_FORMHANDLE hHandle)
{
    FORM_DoDocumentOpenAction(hHandle);
}

DLLEXPORT void STDCALL FPDFDOC_ExitFormFillEnvironment_Wraper(FPDF_FORMHANDLE hHandle)
{
    FPDFDOC_ExitFormFillEnvironment(hHandle);
}

DLLEXPORT void STDCALL FORM_DoDocumentAAction_Wraper(FPDF_FORMHANDLE hHandle, int aaType)
{
    FORM_DoDocumentAAction(hHandle, aaType);
}

DLLEXPORT FPDF_PAGE STDCALL FPDF_LoadPage_Wraper(FPDF_DOCUMENT document, int page_index)
{
    return FPDF_LoadPage(document, page_index);
}

DLLEXPORT FPDF_TEXTPAGE STDCALL FPDFText_LoadPage_Wraper(FPDF_PAGE page)
{
    return FPDFText_LoadPage(page);
}

DLLEXPORT void STDCALL FORM_OnAfterLoadPage_Wraper(FPDF_PAGE page, FPDF_FORMHANDLE hHandle)
{
    FORM_OnAfterLoadPage(page, hHandle);
}

DLLEXPORT void STDCALL FORM_DoPageAAction_Wraper(FPDF_PAGE page, FPDF_FORMHANDLE hHandle, int aaType)
{
    FORM_DoPageAAction(page, hHandle, aaType);
}

DLLEXPORT double STDCALL FPDF_GetPageWidth_Wraper(FPDF_PAGE page)
{
    return FPDF_GetPageWidth(page);
}

DLLEXPORT double STDCALL FPDF_GetPageHeight_Wraper(FPDF_PAGE page)
{
    return FPDF_GetPageHeight(page);
}

DLLEXPORT void STDCALL FORM_OnBeforeClosePage_Wraper(FPDF_PAGE page, FPDF_FORMHANDLE hHandle)
{
    FORM_OnBeforeClosePage(page, hHandle);
}

DLLEXPORT void STDCALL FPDFText_ClosePage_Wraper(FPDF_TEXTPAGE text_page)
{
    FPDFText_ClosePage(text_page);
}

DLLEXPORT void STDCALL FPDF_ClosePage_Wraper(FPDF_PAGE page)
{
    FPDF_ClosePage(page);
}

DLLEXPORT void STDCALL FPDF_RenderPageBitmap_Wraper(FPDF_BITMAP bitmap, FPDF_PAGE page, int start_x, int start_y, int size_x, int size_y, int rotate, int flags)
{
    FPDF_RenderPageBitmap(bitmap, page, start_x, start_y, size_x, size_y, rotate, flags);
}

DLLEXPORT int STDCALL FPDF_GetPageSizeByIndex_Wraper(FPDF_DOCUMENT document, int page_index, double* width, double* height)
{
    return FPDF_GetPageSizeByIndex(document, page_index, width, height);
}

DLLEXPORT FPDF_BITMAP STDCALL FPDFBitmap_CreateEx_Wraper(int width, int height, int format, void* first_scan, int stride)
{
    return FPDFBitmap_CreateEx(width, height, format, first_scan, stride);
}

DLLEXPORT void STDCALL FPDFBitmap_FillRect_Wraper(FPDF_BITMAP bitmap, int left, int top, int width, int height, FPDF_DWORD color)
{
    FPDFBitmap_FillRect(bitmap, left, top, width, height, color);
}

DLLEXPORT void STDCALL FPDFBitmap_Destroy_Wraper(FPDF_BITMAP bitmap)
{
    FPDFBitmap_Destroy(bitmap);
}

DLLEXPORT FPDF_SCHHANDLE STDCALL FPDFText_FindStart_Wraper(FPDF_TEXTPAGE text_page, FPDF_WIDESTRING findwhat, unsigned long flags, int start_index)
{
    return FPDFText_FindStart(text_page, findwhat, flags, start_index);
}

DLLEXPORT int STDCALL FPDFText_GetSchResultIndex_Wraper(FPDF_SCHHANDLE handle)
{
    return FPDFText_GetSchResultIndex(handle);
}

DLLEXPORT int STDCALL FPDFText_GetSchCount_Wraper(FPDF_SCHHANDLE handle)
{
    return FPDFText_GetSchCount(handle);
}

DLLEXPORT int STDCALL FPDFText_GetText_Wraper(FPDF_TEXTPAGE text_page, int start_index, int count, unsigned short* result)
{
    return FPDFText_GetText(text_page, start_index, count, result);
}

DLLEXPORT void STDCALL FPDFText_GetCharBox_Wraper(FPDF_TEXTPAGE text_page, int index, double* left, double* right, double* bottom, double* top)
{
    FPDFText_GetCharBox(text_page, index, left, right, bottom, top);
}

DLLEXPORT FPDF_BOOL STDCALL FPDFText_FindNext_Wraper(FPDF_SCHHANDLE handle)
{
    return FPDFText_FindNext(handle);
}

DLLEXPORT void STDCALL FPDFText_FindClose_Wraper(FPDF_SCHHANDLE handle)
{
    FPDFText_FindClose(handle);
}


